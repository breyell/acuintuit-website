# WordPress Starter Theme

## Theme Overview

In `vue.config.js`, update the theme name in the path on line 4.

Using `.env.example` as a reference, create a `.env` file and fill in the ACF Pro key.

In `src/sass/core/_settings.scss`, `$dev-mode` should be set to `false` before the final deploy to production.

## Project setup

```
composer install
yarn
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
