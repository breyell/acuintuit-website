<?php

/* Template Name: Events Page */

$context = Timber::get_context();
$context['events'] = Timber::get_posts([
    'post_type' => 'events',
    'post_status' => 'publish',
    'meta_key'  => 'date_custom',
    'orderby'   => 'meta_value',
    'order'     => 'ASC',
    'meta_query' => [
        'key'       => 'date_custom',
        'value'     => date("Ymd"),
        'type'      => 'NUMERIC',
        'compare'   => '>=',
    ],
]);

Timber::render('templates/events.twig', $context);
