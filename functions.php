<?php

use \Dotenv\Dotenv;
use \Timber\Timber;
use \Timber\Image;
use \Timber\Menu;

require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

new Timber();

Timber::$dirname = 'src/views';

function agen_timber_context()
{
    $context = Timber::get_context();
    $context['post'] = Timber::get_post();
    $context['global'] = get_fields('options');
    $context['env'] = $_ENV['WP_ENV'];
    $context['header_menu'] = new Menu('Header');
    $context['footer_menu'] = new Menu('Footer');

    return $context;
}
add_filter('timber/context', 'agen_timber_context');

function agen_admin_menu()
{
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
    //     remove_menu_page('edit.php?post_type=acf-field-group');
}
add_filter('admin_menu', 'agen_admin_menu');

function register_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu', __('Footer Menu'));
}
add_action('init', 'register_menu');

function add_svg_to_upload_mimes($upload_mimes)
{
    $upload_mimes['svg'] = 'image/svg';
    $upload_mimes['svgz'] = 'image/svg';

    return $upload_mimes;
}
add_filter('upload_mimes', 'add_svg_to_upload_mimes', 10, 1);


// function tiny_mce_remove_unused_formats( $init ) {
// 	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;';
// 	return $init;
// }
// add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );

// function remove_tiny_mce_buttons_from_editor( $buttons ) {
//     $remove_buttons = array(
//         'bold',
//         'italic',
//         'strikethrough',
//         'bullist',
//         'numlist',
//         'blockquote',
//         'hr', // horizontal line
//         'alignleft',
//         'aligncenter',
//         'alignright',
//         'link',
//         'unlink',
//         'wp_more', // read more link
//         'spellchecker',
//         'fullscreen',
//         'dfw', // distraction free writing mode
//         'wp_adv', // kitchen sink toggle (if removed, kitchen sink will always display)
//     );
//     foreach ( $buttons as $button_key => $button_value ) {
//         if ( in_array( $button_value, $remove_buttons ) ) {
//             unset( $buttons[ $button_key ] );
//         }
//     }
//     return $buttons;
// }
// add_filter( 'mce_buttons', 'remove_tiny_mce_buttons_from_editor');

function add_image_sizes()
{
    add_image_size('small_custom', 512, 512);
    add_image_size('medium_custom', 768, 768);
    add_image_size('large_custom', 1024, 1024);
    add_image_size('xlarge_custom', 1280, 1280);
    add_image_size('xxlarge_custom', 1920, 1920);
    add_image_size('xxxlarge_custom', 3000, 3000);
}
add_action('after_setup_theme', 'add_image_sizes');

// Disable Gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);

function add_global_options_page()
{
    acf_add_options_page([
        'page_title'    => 'Global Settings',
        'menu_title'    => 'Global Settings',
        'menu_slug'     => 'global-settings',
        'capability'    => 'edit_posts',
        'redirect'      => true,
        'icon_url'      => 'dashicons-admin-site',
        'position'      => 9
    ]);
}
add_action('acf/init', 'add_global_options_page');

function create_testimonial_post_type()
{
    register_post_type(
        'testimonial-post',
        array(
            'labels' => array(
                'name' => __('Testimonials'),
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-format-quote',
        )
    );
}
add_action('init', 'create_testimonial_post_type');

function create_events_post_type()
{
    register_post_type(
        'events',
        array(
            'labels' => array(
                'name' => __('Events'),
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-calendar-alt',
            'publicly_queryable'  => false,
        )
    );
}
add_action('init', 'create_events_post_type');

function get_twig(Twig_Environment $twig)
{
    $twig->addFunction(new Twig_Function('image', function ($image, $attributes) {
        if (!(is_array($attributes) || is_string($attributes))) {
            throw new TypeError;
        }

        $image = new Image($image);

        $image->srcset = implode(', ', array_map(function ($size) use ($image) {
            $file = explode('/', $image->file);

            return str_replace($file[count($file) - 1], $size['file'], $image->src . ' ' . $size['width'] . 'w');
        }, $image->sizes));

        if (is_string($attributes)) {
            $attributes = ['class' => $attributes];
        }

        $attributes = array_merge($attributes, [
            'src' => $image->src,
            'alt' => $image->alt,
        ]);

        if ($image->srcset) {
            $attributes['srcset'] = $image->srcset;
        }

        $data = [
            'attributes' => $attributes,
        ];

        $html = '<img';

        foreach ($attributes as $name => $value) {
            $html .= $value === null
                ? ' ' . $name
                : ' ' . $name . '="' . $value . '"';
        }

        $html .= '>';

        return $html;
    }));

    return $twig;
}
add_filter('get_twig', 'get_twig');
