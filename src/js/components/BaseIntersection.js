export default {
  props: {
    tag: {
      type: String,
      default: 'div',
    },
    callback: {
      type: Function,
      default: null,
    },
    options: {
      type: Object,
      default: () => ({}),
    },
  },

  data: () => ({
    intersectionObserver: undefined,
  }),

  watch: {
    options: 'reset',
    callback: 'reset',
  },

  mounted() {
    this.create()
    this.observe()
  },

  methods: {
    create() {
      this.intersectionObserver = new IntersectionObserver(
        ([entry]) => this.callback(entry),
        this.options
      )
    },

    observe() {
      this.intersectionObserver.observe(this.$el)
    },

    disconnect() {
      this.intersectionObserver.disconnect()
    },

    reset() {
      this.disconnect()
      this.create()
      this.observe()
    },
  },

  render(createElement) {
    return createElement(this.tag, this.$slots.default)
  },
}
