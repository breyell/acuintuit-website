import { Power0 } from 'gsap'

export default {
  methods: {
    marquee() {
      const { innerWidth } = window
      const { offsetWidth } = this.$el
      this.animationFromTo(
        {
          x: this.$vnode.key === 1 ? -(offsetWidth - innerWidth) : innerWidth,
        },
        {
          x: `-=${offsetWidth}`,
          repeat: -1,
          ease: Power0.easeNone,
        }
      )
    },
  },
}
