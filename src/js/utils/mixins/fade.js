export default {
  methods: {
    fadeIn() {
      this.animationFrom({ opacity: 0 })
    },

    fadeOut() {
      this.animationTo({ opacity: 0 })
    },
  },
}
