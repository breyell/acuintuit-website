export default {
  methods: {
    rotateFrom() {
      this.animationFrom({
        opacity: this.opacity,
        rotation: `${this.rotation}deg`,
        transformOrigin: '50% 50%',
        ease: this.ease,
      })
    },

    rotateTo() {
      this.animationTo({
        opacity: this.opacity,
        rotation: `${this.rotation}deg`,
        ease: this.ease,
      })
    },
  },
}
