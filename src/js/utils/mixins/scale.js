export default {
  methods: {
    scaleFrom() {
      this.animationFrom({
        opacity: this.opacity,
        scale: this.scale,
        transformOrigin: '50% 50%',
        ease: this.ease,
      })
    },

    scaleTo() {
      this.animationTo({
        opacity: this.opacity,
        scale: this.scale,
        ease: this.ease,
      })
    },
  },
}
