export default {
  methods: {
    slideFromX() {
      this.animationFrom({
        opacity: this.opacity,
        x: this.distance,
        ease: this.ease,
      })
    },

    slideFromXPercent() {
      this.animationFrom({
        opacity: this.opacity,
        xPercent: this.distance,
        ease: this.ease,
      })
    },

    slideFromY() {
      this.animationFrom({
        opacity: this.opacity,
        y: this.distance,
        ease: this.ease,
      })
    },

    slideFromYPercent() {
      this.animationFrom({
        opacity: this.opacity,
        yPercent: this.distance,
        ease: this.ease,
      })
    },

    slideToX() {
      this.animationTo({
        opacity: this.opacity,
        x: this.distance,
        ease: this.ease,
      })
    },

    slideToXPercent() {
      this.animationTo({
        opacity: this.opacity,
        xPercent: this.distance,
        ease: this.ease,
      })
    },

    slideToY() {
      this.animationTo({
        opacity: this.opacity,
        y: this.distance,
        ease: this.ease,
      })
    },

    slideToYPercent() {
      this.animationTo({
        opacity: this.opacity,
        yPercent: this.distance,
        ease: this.ease,
      })
    },
  },
}
