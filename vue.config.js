module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/wp-content/themes/acuintuit/dist/'
      : '/',

  filenameHashing: false,
  runtimeCompiler: true,
  configureWebpack: {
    performance: {
      hints: false,
    },
  },
  pages: {
    index: {
      entry: 'src/js/main.js',
      chunks: ['chunk-vendors', 'chunk-common', 'index'],
      inject: false,
    },
  },
  devServer: {
    disableHostCheck: true,
  },
  chainWebpack: config => {
    config.plugins.delete('html-index')
    config.plugins.delete('preload-index')
    config.plugins.delete('prefetch-index')
  },
}
